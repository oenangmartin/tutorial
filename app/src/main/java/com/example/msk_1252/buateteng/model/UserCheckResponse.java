package com.example.msk_1252.buateteng.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by msk-1252 on 3/29/16.
 */
public class UserCheckResponse {
    /**
     * serialized name menandakan bahwa ketika ada json yang attribute name nya status, bakal di masukin ke variabel status
     * notifikas ke notifikasi
     * dll
     * sisanya getter setter biasa
     */
    @SerializedName("status")
    private int status;
    @SerializedName("notifikasi")
    private String notifikasi;
    @SerializedName("data")
    private String data;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getNotifikasi() {
        return notifikasi;
    }

    public void setNotifikasi(String notifikasi) {
        this.notifikasi = notifikasi;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
