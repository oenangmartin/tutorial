package com.example.msk_1252.buateteng.interfaces;

import com.example.msk_1252.buateteng.model.UserCheckResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by msk-1252 on 3/29/16.
 */
public interface CheckRequest {
    /**
     * ini retrofit, intinya dari sini ada request tipenya GET,
     * kembaliannya nanti Call gunanya banyak, lol.
     * lalu ini ada query intinya kalo lu liat ada di url aja itu si query :D
     * @param username
     * @return
     */
    @GET("auth/username_check/")
    Call<UserCheckResponse> checkUsername(@Query("username") String username);

    @GET("auth/email_check/")
    Call<UserCheckResponse> checkEmail(@Query("email") String email);

}
