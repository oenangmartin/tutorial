package com.example.msk_1252.buateteng;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.msk_1252.buateteng.interfaces.CheckRequest;
import com.example.msk_1252.buateteng.model.UserCheckResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    String url = "http://barado.co/index.php/api/auth/username_check?username=nancenka";
    private final String END_POINT = "http://barado.co/index.php/api/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /**
         * ini kalo mau pake string request
         * intinya, sebuah request itu cara gimana kita ngambil/masukin data ke server
         * nah request itu ada 2 jenis, yaitu GET sama POST, bedanya kalo GET dipake cuma buat ngambil aja
         * kalo POST dipake buat "masukin" data atau sejenisnya
         * lalu ada url, sama responsenya aja
         * gua pribadi saranin pake string request karena proses parsingnya kita bisa pake library yang ada misal gson
         * dalam kasus ini gua contohin pake gson aja ya :)
         * gson itu intinya ngubah string jadi objek dalam contoh ini gua bikin objek UserCheckResponse
         */
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response", response);
                UserCheckResponse responseModel = MainApplication.gson.fromJson(response, UserCheckResponse.class);
                //silakan diapa-apain ini responsenya :D
                Toast.makeText(MainActivity.this, responseModel.getNotifikasi(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null && error.networkResponse.data != null) {
                    //kalo butuh status codenya
                    Log.e("Error code", String.valueOf(error.networkResponse.statusCode));
                    Log.e("Error", new String(error.networkResponse.data));
                }
            }
        });

        /**
         * pemanggilan pake retrofit
         */

        CheckRequest retrofitRequest = new Retrofit.Builder().baseUrl(END_POINT).
                addConverterFactory(GsonConverterFactory.create()).
                build().create(CheckRequest.class);
        Call<UserCheckResponse> responseCall = retrofitRequest.checkEmail("oenangmartin@gmail.com");
        responseCall.enqueue(new Callback<UserCheckResponse>() {
            @Override
            public void onResponse(Call<UserCheckResponse> call, retrofit2.Response<UserCheckResponse> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Log.d("response email", MainApplication.gson.toJson(response.body()));
                    UserCheckResponse responseModel = response.body();
                    Toast.makeText(MainActivity.this, responseModel.getNotifikasi(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserCheckResponse> call, Throwable t) {
                // kalo gagal
            }
        });
        MainApplication.getInstance().getRequestQueue().add(request);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
